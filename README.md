# Toy Robot Simulator

This is a JavaScript solution to the *Readify Graduate Coding Puzzle*.

Files:

- `robot.js`: main file
- `robot.test.js`: test cases
- `demo/index.html`: a visualized demonstration

## How to run

The library itself has no dependency.

This simulator can run in a web browser __or__ node.js environment. 

Tested with the latest stable version of Chrome and node.js.

## Uasge 


```javascript
const robot = new Robot();

robot.exec(`Some commands`);
```


## Testing

### In node.js

Install the testing dependencies by running `npm install`.

Run test cases by `npm test`.

### Manual test in browsers

`demo/index.html` is a visualized demostration and user interface. 
You can manually type in some commands and see how each step is executed. You can also get some randomly generated commands by hitting the `Random!` button, and execute with `Run!`.


## Author 

Wentao Liu (liuwnt@gmail.com)