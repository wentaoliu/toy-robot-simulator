/*
 * Demo
 *
 * Author: Wentao Liu <liuwnt@gmail.com>
 * Date: 30/06/2019
 * 
 * A visualised demonstration of `robot.js`.
 */
(function() {
  // DOM elements
  const eleResult = document.getElementById("result");
  const btnRandom = document.getElementById("random");
  const btnRun = document.getElementById("run");

  const directions = ["SOUTH", "NORTH", "EAST", "WEST"];
  const commands = ["LEFT", "RIGHT", "MOVE"];

  // Randomly generate some commands
  btnRandom.addEventListener("click", () => {
    let ran = randomInt(2);
    let dir = directions[ran];
    let lines = `PLACE ${randomInt(5)},${randomInt(5)},${dir}\n`;
    for (let i = 0; i < 15; i++) {
      ran = randomInt(3);
      lines += `${commands[ran]}\n`;
    }
    lines += "REPORT\n";

    document.getElementById("commands").value = lines;
  });

  // Execute the commands
  btnRun.addEventListener("click", () => {
    btnRun.disabled = true;
    btnRandom.disabled = true;
    // Clear previous results
    eleResult.innerHTML = "";
    const tds = document.getElementsByClassName("cell");
    for (let td of tds) {
      td.innerHTML = "";
    }

    const lines = document.getElementById("commands").value;
    const cmds = lines.split(/\r?\n/);

    const interval = 800; // animation interval (ms)
    let x, y, dir;
    let robot = new Robot(); // initialize a new Robot object.
    cmds.forEach((cmd, i) => {
      setTimeout(() => {
        robot.execCommand(cmd);

        x = robot.posX;
        y = robot.posY;
        dir = robot.facingDirection;

        if (x >= 0 && y >= 0 && dir) {
          let cell = document.getElementById(`x${x}-y${y}`);
          let robotNode = createRobotNode(dir);
          cell.innerHTML = "";
          cell.appendChild(robotNode);
        }

        if(i === cmds.length - 1) {
          btnRun.disabled = false;
          btnRandom.disabled = false;
        }
      }, i * interval);
    });
  });

  function createLogNode(text) {
    const node = document.createElement("p");
    node.innerText = text;
    node.classList.add("log");
    return node;
  }

  function createErrorNode(text) {
    const node = document.createElement("p");
    node.innerText = text;
    node.classList.add("error");
    return node;
  }

  function createRobotNode(dir) {
    const node = document.createElement("div");
    node.classList.add(dir);
    return node;
  }

  // Redirect all log and error messages.
  console.log = function() {
    eleResult.appendChild(createLogNode(Array.from(arguments)));
  };
  console.error = function() {
    eleResult.appendChild(createErrorNode(Array.from(arguments)));
  };

  function randomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  // This command will leave a trajectory in the shape of 'W'
  const initCmd = `PLACE 0,4,SOUTH
MOVE
MOVE
MOVE
MOVE
LEFT
MOVE
MOVE
LEFT
MOVE
MOVE
MOVE
MOVE
RIGHT
RIGHT
MOVE
MOVE
MOVE
MOVE
LEFT
MOVE
MOVE
LEFT
MOVE
MOVE
MOVE
MOVE
REPORT`;

  document.getElementById("commands").value = initCmd;
})();
