/*
 * Toy Robot Simulator
 *
 * Author: Wentao Liu <liuwnt@gmail.com>
 * Date: 30/06/2019
 *
 * A robot class represent a robot moving on a tabletop.
 * The position of a robot is represented by a 2-dimensional coordinate (x,y).
 * A robot is also facing a direction (one of North, South, West, or East).
 *
 * A robot can taken a series of commands, consisting of:
 * - PLACE X,Y,DIRECTION
 * - MOVE
 * - LEFT
 * - RIGHT
 * - REPORT
 *
 * Execute commands by calling `exec()`;
 * Get the current coordinate and facing directoin via getters:
 * `posX`, `posY` and `facingDirection`.
 */

class Robot {
  // By default, the map is a 5x5 matrix
  constructor(width = 5, height = 5) {
    if (width < 1 && height < 1) {
      throw new Error("Width or height cannot be negative.");
    }
    this.width = width;
    this.height = height;
    // initalize the position with an unreachable coordinate
    this.x = -1;
    this.y = -1;
    this.facing = null;

    // We represent the direction by a tuple (2-element array)
    // to simplify calculation.
    this.dirs = {
      NORTH: [0, 1],
      SOUTH: [0, -1],
      WEST: [-1, 0],
      EAST: [1, 0]
    };
  }

  // This is the main function should be called from outside.
  // Each command should start in a new line.
  exec(lines) {
    const cmds = lines.split(/\r?\n/);
    cmds.forEach(cmd => {
      this.execCommand(cmd);
    });
  }

  execCommand(line) {
    const [cmd, ...args] = line.trim().split(/,| /);
    // Since the leftmost tokens (of all valid commands) are disjoint, 
    // A single switch statement is enough to parse the commands.
    switch (cmd) {
      case "PLACE":
        this.place(args);
        break;
      case "MOVE":
        this.move();
        break;
      case "LEFT":
        this.rotateLeft();
        break;
      case "RIGHT":
        this.rotateRight();
        break;
      case "REPORT":
        this.report();
        break;
      case "": // ignore empty commands
        break;
      default:
        // for unknown commands, ignore and
        console.error(`Unknown command: ${cmd}`);
    }
  }

  place(args) {
    // Since PLACE command takes 3 arguments,
    // we need to validate those arguments first.
    if (args.length !== 3) {
      console.error(`PLACE: expected 3 arguments, but got ${args.length}`);
      return;
    }
    let [x, y, dir] = args;
    x = parseInt(x, 10);
    y = parseInt(y, 10);
    if (!this.validPos(x, y)) {
      console.error("PLACE: invalid position");
      return;
    }
    if (!Object.keys(this.dirs).includes(dir)) {
      console.error("PLACE: invalid direction");
      return;
    }
    this.x = x;
    this.y = y;
    this.facing = this.dirs[dir];
  }

  // Make sure a robot won't step outside the map.
  validPos(x, y) {
    return x >= 0 && x < this.width && y >= 0 && y < this.height;
  }

  // Move the robot one step forward in its facing direction.
  move() {
    if (!this.facing) {
      console.error("MOVE: place the robot on the map first.");
      return;
    }
    let [x, y] = this.facing;
    x += this.x;
    y += this.y;
    if (this.validPos(x, y)) {
      this.x = x;
      this.y = y;
    } else {
      console.error("MOVE: falling to destruction");
    }
  }

  // Since the direction is stored in a form of coordinate, we only need to do 
  // a coordinate manipulation to rotate it. (no switch-case needed).
  // (x,y) -> (-y,x) after rotation of 90 degree (counterclockwise)
  rotateLeft() {
    if (!this.facing) {
      console.error("LEFT: place the robot on the map first.");
      return;
    }
    const [x, y] = this.facing;
    this.facing = [-y, x];
  }

  // (x,y) -> (y,-x) after rotation of 90 degree (clockwise)
  rotateRight() {
    if (!this.facing) {
      console.error("RIGHT: place the robot on the map first.");
      return;
    }
    const [x, y] = this.facing;
    this.facing = [y, -x];
  }

  // print current state (position and facing direction)
  report() {
    if (this.x > -1 && this.y > -1 && this.facing) {
      const state = [this.x, this.y, this.facingDirection].join(",");
      console.log(state);
    } else {
      console.error(
        "REPORT: The robot hasn't been placed yet! Nothing to report."
      );
    }
  }

  // Since we store the facing direction as an array internally,
  // we need a getter function to retrive the direction as strings.
  get facingDirection() {
    if (!this.facing) return null;
    return Object.keys(this.dirs).find(
      key =>
        this.dirs[key][0] == this.facing[0] &&
        this.dirs[key][1] == this.facing[1]
    );
  }

  get posX() {
    return this.x;
  }

  get posY() {
    return this.y;
  }
}

// Export for both node.js and browsers.
if (typeof module !== "undefined" && typeof module.exports !== "undefined")
  module.exports = Robot;
else window.Robot = Robot;
