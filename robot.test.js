const Robot = require("./robot");

let robot = new Robot();

beforeEach(() => {
  robot = new Robot();
});

test("Can place and move", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 0,0,NORTH
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("0,1,NORTH");
  spy.mockRestore();
});

test("Can place and rotate", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 0,0,NORTH
    LEFT
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("0,0,WEST");
  spy.mockRestore();
});

test("Can place, move and rotate", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("3,3,NORTH");
  spy.mockRestore();
});

test("Will not fall (East)", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE 2,2,EAST
    MOVE
    MOVE
    MOVE
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("MOVE: falling to destruction");
  spy.mockRestore();
});


test("Will not fall (West)", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE 2,2,WEST
    MOVE
    MOVE
    MOVE
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("MOVE: falling to destruction");
  spy.mockRestore();
});



test("Will not fall (North)", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE 2,2,NORTH
    MOVE
    MOVE
    MOVE
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("MOVE: falling to destruction");
  spy.mockRestore();
});



test("Will not fall (South)", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE 2,2,SOUTH
    MOVE
    MOVE
    MOVE
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("MOVE: falling to destruction");
  spy.mockRestore();
});


test("Can handle invalid arguments", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE EAST,2,2
  `);

  expect(spy).toHaveBeenCalledWith("PLACE: invalid position");
  spy.mockRestore();
});


test("Can handle invalid number of arguments", () => {
  const spy = jest.spyOn(console, "error");
  robot.exec(`
    PLACE 2,2,East,2,2
  `);

  expect(spy).toHaveBeenCalledWith("PLACE: expected 3 arguments, but got 5");
  spy.mockRestore();
});

test("Offical Example a)", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 0,0,NORTH
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("0,1,NORTH");
  spy.mockRestore();
});

test("Can handle invalid number of arguments", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 0,0,NORTH
    LEFT
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("0,0,WEST");
  spy.mockRestore();
});

test("Can handle invalid number of arguments", () => {
  const spy = jest.spyOn(console, "log");
  robot.exec(`
    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT
  `);

  expect(spy).toHaveBeenCalledWith("3,3,NORTH");
  spy.mockRestore();
});